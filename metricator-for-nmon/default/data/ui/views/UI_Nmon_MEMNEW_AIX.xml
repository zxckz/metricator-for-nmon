<form script="ui_simple.js" stylesheet="ui_simple.css,panel_decoration.css" isVisible="true">
    <label>UI MEMNEW, AIX Memory Allocation Analysis</label>
    <description>User Interface for the MEMNEW monitor</description>

    <fieldset autoRun="false" submitButton="false">

        <input type="time" token="timerange" searchWhenChanged="true">
            <label>Time Range:</label>
            <default>
                <earliestTime>-24h</earliestTime>
                <latestTime>now</latestTime>
            </default>
		    <change>
		        <unset token="form.refresh"></unset>
		    </change>
        </input>

        <input type="dropdown" token="timefilter" searchWhenChanged="true">
            <label>Time Filtering:</label>
            <choice value="No_Filter">No Filter (24/24, 7/7)</choice>
            <choice value="Day_BusinessDays_8h-19h">Day Business (08h-19h)</choice>
            <choice value="Day_WeekEnd_8h-19h">Day WE (08h-19h)</choice>
            <choice value="Day_AllDays_8h-19h">Day Week (08h-19h)</choice>
            <choice value="Night_BusinessDays_19h-8h">Night Business (19h-08h)</choice>
            <choice value="Night_WeekEnd_19h-8h">Night WE (19h-08h)</choice>
            <choice value="Night_AllDays_19h-8h">Night All Days (19h-08h)</choice>
            <default>No_Filter</default>
        </input>

        <input id="frameID" type="multiselect" token="frameID" searchWhenChanged="true">
            <label>Frame IDs:</label>
            <!-- Populating Data Model Search -->
            <search base="populate">
                <query>stats count by frameID | dedup frameID | sort 0 frameID</query>
            </search>
            <valuePrefix>frameID=</valuePrefix>
            <delimiter> OR </delimiter>
            <choice value="*">Any</choice>
            <default>*</default>
            <fieldForLabel>frameID</fieldForLabel>
            <fieldForValue>frameID</fieldForValue>
        </input>

        <input type="text" token="host-prefilter" searchWhenChanged="true">
            <label>Optional: Filter hosts populating</label>
            <default>*</default>
        </input>

		<input id="host" type="multiselect" token="host" searchWhenChanged="true">
		    <label>Hosts Selection:</label>
		    <!-- Populating Data Model Search -->
		    <search base="populate">
		        <query>search $frameID$ host=$host-prefilter$
	| stats count by host | dedup host | sort 0 host</query>
		    </search>
		    <valuePrefix>host="</valuePrefix>
		    <valueSuffix>"</valueSuffix>
		    <delimiter> OR </delimiter>
		    <choice value="*">ALL Hosts</choice>
		    <fieldForLabel>host</fieldForLabel>
		    <fieldForValue>host</fieldForValue>
		    <change>
		        <condition>
		            <unset token="form.host_query_input"></unset>
		        </condition>
		    </change>
		</input>

		<!-- hidden form that handles the advanced definition of hosts selection -->

		<input id="host_query_input" type="dropdown" token="host_query" searchWhenChanged="true" depends="$hidden_form$">
		    <label>Hosts Selection:</label>
		    <search base="populate">
		        <query>search $host$ $frameID$
	| stats values(host) as host
	| format | fields - host | rename search as host</query>
		    </search>
		    <selectFirstChoice>true</selectFirstChoice>
		    <fieldForLabel>host</fieldForLabel>
		    <fieldForValue>host</fieldForValue>
		</input>

        <input type="dropdown" token="statsmode" searchWhenChanged="true">
            <label>Select a stats mode:</label>
            <default>max</default>
            <choice value="max">Max</choice>
            <choice value="avg">Avg</choice>
            <choice value="min">Min</choice>
            <choice value="median">Median</choice>
            <choice value="mode">Mode</choice>
            <choice value="range">Range</choice>
        </input>

        <input type="dropdown" token="refresh" searchWhenChanged="true">
            <label>Auto-refresh:</label>
            <fieldForLabel>label</fieldForLabel>
            <fieldForValue>value</fieldForValue>
            <selectFirstChoice>true</selectFirstChoice>
            <search>
                <query>| `def_auto_refresh`</query>
                <earliest>$timerange.earliest$</earliest>
                <latest>$timerange.latest$</latest>
            </search>
        </input>

    </fieldset>

    <!--
    Dynamic configuration
    The following searches are being used to define various tokens using event handlers
    -->

    <!-- Base Searches for PostProcessing -->

    <search id="populate">
        <query>| mcatalog values(metric_name) as metric_name values(OStype) as OStype WHERE `nmon_metrics_index` metric_name="os.unix.nmon.memory.memnew.*" Ostype=AIX groupby host
| `mapping_frameID`
| stats count by frameID, host | sort 0 host</query>
        <earliest>$timerange.earliest$</earliest>
        <latest>$timerange.latest$</latest>
    </search>

    <!-- Help the user -->

    <row rejects="$host$">
        <panel>
            <html>
                <div class="red_help_user">
                    <p>- - - - - - - - - - ACTION REQUIRED: please select your server name(s) in the host selector above - - - - - - - - - -</p>
                </div>
            </html>
        </panel>
    </row>

    <!-- Information panel -->

    <row>
        <panel>
            <html>

                <div class="imgheader">
                    <img src="../../static/app/metricator-for-nmon/icons/grey_theme/info.png" alt="Info"/>
                    <h4><a data-toggle="modal" data-target="#Help_modal">Help, information and related links for Memory statistics</a></h4>
                </div>

                <!-- Modal -->
                <div class="modal custom-modal-60 fade" id="Help_modal" tabindex="-1" role="dialog" aria-labelledby="Help_modal_Label">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
                                <div style="text-align: left;">
                                    <h4 class="modal-title" id="Help_modal_Label">Integrated Navigation:</h4>
                                </div>
                            </div>
                            <div class="modal-body">

                                <div style="text-align: center;">
                                    <img src="../../static/app/metricator-for-nmon/icons/grey_theme/memory.png" alt="memory"/>
                                    <h1>AIX Memory Allocation
                                        <br />(os.unix.nmon.memory.memnew.*)</h1>
                                </div>

                                <div style="text-align: left;">

                                    <h1>Main metrics/fields:</h1>

                                    <div class="list">
                                        <lu>
                                            <li><b>Process:</b> Percentage of real memory that is used by process segments</li>
                                            <li><b>System:</b> Percentage of real memory that is used by system segments</li>
                                            <li><b>Free:</b> Percentage of real memory that is free</li>
                                            <li><b>FScache:</b> Percentage of real memory that is used for File System Cache</li>
                                        </lu>
                                    </div>

                                </div>

                                <div style="text-align:left;">
                                    <h1>Related links:</h1>
                                </div>

                                <div style="text-align: center;" class="cat_title">

                                    <a target="_blank" href="search?q=%7C%20mcatalog%20values(metric_name)%20as%20metric_name%20where%20%60nmon_metrics_index%60%20metric_name%3Dos.unix.nmon.memory.memnew.*%20host%3D*%20(OStype%3DAIX)%20" class="tryitbtnxl">mcatalog »
                                    </a>

                                    <a target="_blank" href="search?q=%7C%20mstats%20avg(_value)%20as%20value%20where%20%60nmon_metrics_index%60%20metric_name%3Dos.unix.nmon.memory.memnew.*%20host%3D*%20(OStype%3DAIX)%20groupby%20metric_name%2C%20host%20span%3D1s" class="tryitbtnxl">mstats raw query»
                                    </a>

                                    <a target="_blank" href="search?q=%7C%20mstats%20avg(_value)%20as%20value%20where%20%60nmon_metrics_index%60%20metric_name%3Dos.unix.nmon.memory.memnew.*%20host%3D*%20groupby%20metric_name%2C%20host%20span%3D1s%0A%7C%20%60extract_metrics(&quot;Free_PCT%20FScache_PCT%20memused_PCT%20Process_PCT%20System_PCT&quot;)%60%0A%7C%20stats%20values(Free_PCT)%20as%20Free_PCT%2C%20values(FScache_PCT)%20as%20FScache_PCT%2C%20values(Process_PCT)%20as%20Process_PCT%2C%20values(System_PCT)%20as%20System_PCT%20by%20_time%2C%20host%0A%7C%20eval%20memused_PCT%3D(FScache_PCT%2BProcess_PCT%2BSystem_PCT)" class="tryitbtnxl">mstats pre-built query »
                                    </a>

                                    <a target="_blank" href="Howto_MEM_spl" class="tryitbtnxl">HOWTO Interface »
                                    </a>

                                    <a target="_blank" href="UI_data_dictionary?form.osfilter=AIX%3Dtrue&amp;form.itemfilter=MEMORY%20STATISTICS" class="tryitbtnxl">Memory Data Dictionary »
                                    </a>

                                    <a target="_blank" href="UI_Nmon_TOP_AIX" class="tryitbtnxl">TOP interface »
                                    </a>

                                </div>

                                <div style="text-align:left;">
                                    <h1>Related metrics:</h1>
                                </div>

                                <div style="text-align: center;" class="cat_title">

                                    <a target="_blank" href="search?q=%7C%20mstats%20avg(_value)%20as%20value%20where%20%60nmon_metrics_index%60%20metric_name%3Dos.unix.nmon.memory.mem.*%20OStype%3DAIX%20host%3D*%20groupby%20metric_name%2C%20host%20span%3D1s" class="tryitbtnxl">os.unix.nmon.memory.mem.* »
                                    </a>

                                    <a target="_blank" href="search?q=%7C%20mstats%20avg(_value)%20as%20value%20where%20%60nmon_metrics_index%60%20metric_name%3Dos.unix.nmon.memory.memuse.*%20OStype%3DAIX%20host%3D*%20groupby%20metric_name%2C%20host%20span%3D1s" class="tryitbtnxl">os.unix.nmon.memory.memuse.* »
                                    </a>

                                    <a target="_blank" href="search?q=%7C%20mstats%20avg(_value)%20as%20value%20where%20%60nmon_metrics_index%60%20metric_name%3Dos.unix.nmon.cpu.lpar.*%20OStype%3DAIX%20host%3D*%20groupby%20metric_name%2C%20host%20span%3D1s" class="tryitbtnxl">os.unix.nmon.cpu.lpar.* »
                                    </a>

                                    <a target="_blank" href="search?q=%7C%20mstats%20avg(_value)%20as%20value%20where%20%60nmon_metrics_index%60%20metric_name%3Dos.unix.nmon.processes.top.*%20OStype%3DAIX%20host%3D*%20groupby%20metric_name%2C%20host%20span%3D1s" class="tryitbtnxl">os.unix.nmon.cpu.top.* »
                                    </a>

                                    <a target="_blank" href="search?q=%7C%20mstats%20avg(_value)%20as%20value%20where%20%60nmon_metrics_index%60%20metric_name%3Dos.unix.nmon.cpu.cpu_all.*%20OStype%3DAIX%20host%3D*%20groupby%20metric_name%2C%20host%20span%3D1s" class="tryitbtnxl">os.unix.nmon.cpu.cpu_all.* »
                                    </a>

                                    <a target="_blank" href="search?q=%7C%20mstats%20avg(_value)%20as%20value%20where%20%60nmon_metrics_index%60%20metric_name%3Dos.unix.nmon.cpu.cpunn.*%20OStype%3DAIX%20host%3D*%20groupby%20metric_name%2C%20host%20span%3D1s" class="tryitbtnxl">os.unix.nmon.cpu.cpunn.* »
                                    </a>

                                </div>

                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>

                        </div>
                    </div>
                </div>

            </html>
        </panel>
    </row>

    <row>
        <panel>
            <title>Table Stats</title>
            <table>
                <search>
                    <query>| mstats avg(_value) as value where `nmon_metrics_index` metric_name="os.unix.nmon.memory.memnew.*" $host_query$ groupby metric_name, host span=1s
| `$timefilter$`
| `extract_metrics("Free_PCT FScache_PCT memused_PCT Process_PCT System_PCT")`
| stats values(Free_PCT) as Free_PCT, values(FScache_PCT) as FScache_PCT, values(Process_PCT) as Process_PCT, values(System_PCT) as System_PCT by _time, host
| fillnull value=0 FScache_PCT, Process_PCT, System_PCT
| eval memused_PCT=(FScache_PCT+Process_PCT+System_PCT)
| stats avg(Free_PCT) AS Free_PCT avg(FScache_PCT) AS FScache_PCT avg(memused_PCT) AS memused_PCT avg(Process_PCT) AS Process_PCT avg(System_PCT) AS System_PCT by host
| foreach *_PCT [ eval &lt;&lt;FIELD&gt;&gt; = round('&lt;&lt;FIELD&gt;&gt;', 2) ]
| fields host,memused_PCT,Free_PCT,FScache_PCT,Process_PCT,System_PCT</query>
                    <earliest>$timerange.earliest$</earliest>
                    <latest>$timerange.latest$</latest>
                    <refresh>$refresh$</refresh>
                    <refreshType>delay</refreshType>
                </search>
                <option name="wrap">true</option>
                <option name="rowNumbers">false</option>
                <option name="dataOverlayMode">none</option>
                <option name="drilldown">cell</option>
                <option name="count">10</option>
                <option name="refresh.display">none</option>
                <option name="percentagesRow">false</option>
                <format type="color" field="Total Mem Used (%)">
                    <colorPalette type="minMidMax" maxColor="#31A35F" minColor="#FFFFFF"></colorPalette>
                    <scale type="minMidMax"></scale>
                </format>
                <format type="color" field="FScache (%)">
                    <colorPalette type="minMidMax" maxColor="#31A35F" minColor="#FFFFFF"></colorPalette>
                    <scale type="minMidMax"></scale>
                </format>
                <format type="color" field="Process (%)">
                    <colorPalette type="minMidMax" maxColor="#31A35F" minColor="#FFFFFF"></colorPalette>
                    <scale type="minMidMax"></scale>
                </format>
                <format type="color" field="System PCT(%)">
                    <colorPalette type="minMidMax" maxColor="#31A35F" minColor="#FFFFFF"></colorPalette>
                    <scale type="minMidMax"></scale>
                </format>
                <format type="color" field="User (%)">
                    <colorPalette type="minMidMax" maxColor="#31A35F" minColor="#FFFFFF"></colorPalette>
                    <scale type="minMidMax"></scale>
                </format>
            </table>
        </panel>
    </row>

    <row>
        <panel id="settings">
            <title>Charting parameters</title>
            <input type="dropdown" token="chart" searchWhenChanged="true">
                <label>Select a type of chart:</label>
                <default>area</default>
                <choice value="area">Area</choice>
                <choice value="line">Line</choice>
                <choice value="column">Column</choice>
                <choice value="bar">Bar</choice>
            </input>
            <input type="dropdown" token="charting.chart.nullValueMode" searchWhenChanged="true">
                <label>Missing Data:</label>
                <default>gaps</default>
                <choice value="gaps">Gaps</choice>
                <choice value="connect">Connect</choice>
                <choice value="zero">Zero</choice>
            </input>
            <input type="dropdown" token="chart.stackingmode" searchWhenChanged="true">
                <label>Select a stacking mode:</label>
                <default>stacked</default>
                <choice value="stacked">stacked (lines excluded)</choice>
                <choice value="stacked100">100% stacked (lines excluded)</choice>
                <choice value="unstacked">unstacked</choice>
            </input>
            <input type="dropdown" token="charting.legend.placement" searchWhenChanged="true">
                <label>Legend placement:</label>
                <default>bottom</default>
                <choice value="bottom">Bottom</choice>
                <choice value="top">Top</choice>
                <choice value="left">left</choice>
                <choice value="right">right</choice>
                <choice value="none">none</choice>
            </input>
            <input type="dropdown" token="span" searchWhenChanged="true">
                <label>Span:</label>
                <default>`nmon_span`</default>
                <choice value="`nmon_span`">auto</choice>
                <choice value="span=1m">1 minute</choice>
                <choice value="span=2m">2 minutes</choice>
                <choice value="span=3m">3 minutes</choice>
                <choice value="span=4m">4 minutes</choice>
                <choice value="span=5m">5 minutes</choice>
                <choice value="span=10m">10 minutes</choice>
                <choice value="span=15m">15 minutes</choice>
                <choice value="span=30m">30 minutes</choice>
                <choice value="span=1h">1 hour</choice>
                <choice value="span=2h">2 hours</choice>
                <choice value="span=12h">12 hours</choice>
                <choice value="span=4h">4 hours</choice>
                <choice value="span=1d">1 day</choice>
                <choice value="span=2d">2 days</choice>
                <choice value="span=1w">7 days</choice>
                <choice value="span=1mon">1 month</choice>
            </input>
        </panel>
    </row>

    <row>
        <panel id="memory">
            <title>Memory Allocation statistics</title>
            <chart>
                <search>
                    <query>| mstats avg(_value) as value where `nmon_metrics_index` metric_name="os.unix.nmon.memory.memnew.*" $host_query$ groupby metric_name, host span=1s
| `$timefilter$`
| `extract_metrics("Free_PCT FScache_PCT memused_PCT Process_PCT System_PCT")`
| stats values(Free_PCT) as Free_PCT, values(FScache_PCT) as FScache_PCT, values(Process_PCT) as Process_PCT, values(System_PCT) as System_PCT by _time, host
| timechart $span$ avg(Free_PCT) AS Free_PCT avg(FScache_PCT) AS FScache_PCT avg(Process_PCT) AS Process_PCT avg(System_PCT) AS System_PCT by host</query>
                    <earliest>$timerange.earliest$</earliest>
                    <latest>$timerange.latest$</latest>
                    <refresh>$refresh$</refresh>
                    <refreshType>delay</refreshType>
                </search>
                <option name="charting.axisTitleX.visibility">visible</option>
                <option name="charting.axisTitleY.visibility">visible</option>
                <option name="charting.axisX.scale">linear</option>
                <option name="charting.axisY.scale">linear</option>
                <option name="charting.chart">$chart$</option>
                <option name="charting.chart.nullValueMode">$charting.chart.nullValueMode$</option>
                <option name="charting.chart.sliceCollapsingThreshold">0.01</option>
                <option name="charting.chart.stackMode">$chart.stackingmode$</option>
                <option name="charting.chart.style">shiny</option>
                <option name="charting.drilldown">all</option>
                <option name="charting.layout.splitSeries">0</option>
                <option name="charting.legend.labelStyle.overflowMode">ellipsisMiddle</option>
                <option name="charting.legend.placement">$charting.legend.placement$</option>
                <option name="height">680</option>
                <option name="charting.axisTitleX.text">Time</option>
                <option name="charting.axisTitleY.text">Percentage (%)</option>
                <option name="charting.axisY.minimumNumber">0</option>
                <option name="charting.axisY.maximumNumber">100</option>
                <option name="charting.axisLabelsX.majorLabelStyle.overflowMode">ellipsisNone</option>
                <option name="charting.axisLabelsX.majorLabelStyle.rotation">0</option>
                <option name="charting.axisTitleY2.visibility">visible</option>
                <option name="charting.axisY2.enabled">false</option>
                <option name="charting.axisY2.scale">inherit</option>
                <option name="refresh.display">none</option>
            </chart>
        </panel>
    </row>

</form>
