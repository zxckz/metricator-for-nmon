# props.conf

########################
# nmon metrics as events
########################

[nmon_data]

### frameID ###

# frameID mapping:

# The frameID mapping allows to group servers under a frameID label, globally available in every interface of the application
# Using a frameID to logically group servers will facilitate users selection and experience, specially when managing a large number of servers

# Since the release 1.8.4, the mapping is operated using a KVstore collection, which you can manually edit using the provided management interface (see Settings menu)
# The update of the KVstore collection is now operated over the scheduled report "Generate NMON frameID mapping lookup table" which runs every hour by default

# Generate the frameID mapping
LOOKUP-nmon_frameID_mapping = nmon_frameID_mapping serialnum AS serialnum, host as host OUTPUTNEW frameID AS frameID, host_description as host_description

### OStype ###

# OStype is a raw field available in nmon_performance data since V1.6.15, therefore historical data or outdated Add-ons will still required the nmon_inventory mapping
# Automatically create the OStype field from nmon_inventory lookup if it is not already available in raw data

LOOKUP-OStype = nmon_inventory hostname AS hostname OUTPUTNEW OStype AS OStype

# For nmon_data sent over http
[source::nmon_data:http]
KV_MODE=auto

########################################
# CIM normalization / NMON extractions #
########################################

# When applicable, be CIM compliant

# Various CIM
FIELDALIAS-dest = host as dest
EVAL-hypervisor_id = if(isnotnull(frameID), frameID, serialnum)

# UARG: map the command invocations fields with the TOP metric dimension name
FIELDALIAS-dimension_Command = ProgName as dimension_Command, COMM as dimension_Command
FIELDALIAS-dimension_PID = PID as dimension_PID

#
# NMON EXTERNAL
#

# uptime from nmon_external

# extract the load average
EXTRACT-uptime_loadaverage = load[_\-\s]average:\s*(?<load_average_1min>[\d|\.]*);\s*(?<load_average_5min>[\d|\.]*);\s*(?<load_average_15min>[\d|\.]*)

# extract the number of connected Unix users
EXTRACT-nb_users = (?<nb_users>\d*)[\_|\s]user

# uptime
# Sic !!!
EXTRACT-uptime_days = \s{0,}[\d|\w|:]*[\s|_]*up[\s|_]*(?<uptime_days>\d*)[\s|_]*day[s|,|;]*[\s|_]*(?<uptime_dayshour>\d*):(?<uptime_daystime>\d*)
EXTRACT-uptime_days_only_alt = \s*\d\d:\d\d(?:s|AM|PM)\s*up\s*(?<uptime_days>\d*)\s*day
EXTRACT-uptime_days_with_min = \s{0,}[\d|\w|:]*[\s|_]*up[\s|_]*(?<uptime_days>\d*)[\s|_]*day[s|,|;]*[\s|_]*(?<uptime_daysmin>\d*)[\s|_]min
EXTRACT-uptime_days_with_min_alt = \s{0,}\d\d:\d\d(?:s|AM|PM)\s{0,}up\s{0,}(?<uptime_days>\d*)\s*(?:s|day)(?:s|days);\s{0,}(?<uptime_daysmin>\d*)\s{0,}(?:s|hr)(?:s|hrs)
EXTRACT-uptime_minutes = \s{0,}[\d|\w|:]*[\s|_]*up[\s|_]*(?<uptime_minutes>\d*)[\s|_]*min[\s|_]*[,|;]
EXTRACT-uptime_minutes_alt = \s*\d\d:\d\d(?:s|AM|PM)\s*up\s*(?<uptime_minutes>\d*)\s*(?:s|min)(?:s|mins)
EXTRACT-uptime_hours = \s{0,}[\d|\w|:]*[\s|_]*up[\s|_]*(?<uptime_hours>\d*:\d*)[\s|_]*[,|;]
EXTRACT-uptime_hours_alt = \s*\d\d:\d\d(?:s|AM|PM)\s*up\s*(?<uptime_hours>\d*)\s*hr
EXTRACT-uptime_hours_hours = \s{0,}[\d|\w|:]*[\s|_]*up[\s|_]*(?<uptime_hours_hours>\d*):\d*[\s|_]*[,|;]
EXTRACT-uptime_hours_minutes = \s{0,}[\d|\w|:]*[\s|_]*up[\s|_]*\d*:(?<uptime_hours_minutes>\d*)[\s|_]*[,|;]
EVAL-uptime_fromdays = (uptime_days*86400)
EVAL-uptime_fromminutes = (uptime_minutes*60)
EVAL-uptime_fromhours = (uptime_hours_hours*60*60) + (uptime_hours_minutes*60)
EVAL-uptime = case(isnotnull(uptime_daysmin) AND isnotnull(uptime_daysmin), ((uptime_days*86400)+(uptime_daysmin*60)),\
isnotnull(uptime_days) AND isnotnull(uptime_dayshour) AND isnotnull(uptime_daystime), ((uptime_days*86400)+(uptime_dayshour*3600)+(uptime_daystime*60)),\
isnotnull(uptime_minutes), (uptime_minutes*60),\
isnotnull(uptime_hours) AND isnotnull(uptime_hours_hours), (uptime_hours_hours*60*60) + (uptime_hours_minutes*60),\
isnotnull(uptime_hours), (uptime_hours*60*60),\
isnotnull(uptime_days), (uptime_days*86400) )

#################
# nmon processing
#################

[nmon_processing]

# For TA-nmon
EXTRACT-splunk_home = (?i).+\w*\sRoot\sDirectory\s\(\$SPLUNK_HOME\)\:\s{0,}(?P<splunk_home>[a-zA-Z0-9\/\\\-\_\.\:]+)\s

# For nmon-logger
EXTRACT-nmon_home = (?i).+\w*\sRoot\sDirectory\s\(\$NMON_VAR\)\:\s{0,}(?P<nmon_var>[a-zA-Z0-9\/\\\-\_\.\:]+)\s
EXTRACT-operating_system = (?i).+Guest\sOperating\sSystem\:\s{0,}(?P<operating_system>[a-zA-Z0-9]+)\s
EXTRACT-addon_type = (?i).*addon\s*type:\s.*\/(?<addon_type>[a-zA-Z0-9|\-\_]+)\s
EXTRACT-addon_version = (?i).+addon\sversion\:\s{0,}(?P<addon_version>[a-zA-Z0-9\.]+)\s
EXTRACT-python_version = (?i).+Python\sversion\:\s{0,}(?P<python_version>[a-zA-Z0-9\.]+)\s
EXTRACT-perl_version = (?i).+Perl\sversion\:\s{0,}(?P<perl_version>[a-zA-Z0-9\.]+)\s
EVAL-converter_inuse = case(isnotnull(python_version), "Python", isnotnull(perl_version), "Perl")
EVAL-interpreter_version = case(isnotnull(python_version), python_version, isnotnull(perl_version), perl_version)
EXTRACT-nmonparser_version = (?i).+nmonparser\sversion\:\s{0,}(?P<nmonparser_version>[0-9\.]+)\s
EXTRACT-hostname = (?i).+HOSTNAME\:\s{0,}(?P<hostname>[a-zA-Z0-9\-\_\.]+)
EXTRACT-nbr_lines = (?i).+Reading\sNMON\sdata:\s{0,}(?P<nbr_lines>\d+)\slines
EXTRACT-size_in_bytes = (?i).+lines\s(?P<size_in_bytes>\d+)\sbytes
EXTRACT-elapsed_in_seconds = (?i).+Elapsed\stime\swas\:\s{0,}(?P<elapsed_in_seconds>\d+\.\d+)\sseconds
EXTRACT-Nmon_version = (?i).+NMON\sVERSION\:\s{0,}(?P<Nmon_version>[a-zA-Z0-9\-\_\.\s]+)\s
EXTRACT-Time_of_Nmon_data = (?i).+TIME\sof\sNmon\sData\:\s{0,}(?P<Time_of_Nmon_Data>[0-9\:\.]+)\s
EXTRACT-Date_of_Nmon_data = (?i).+DATE\sof\sNmon\sData\:\s{0,}(?P<Date_of_Nmon_Data>[a-zA-Z0-9\-\/]+)\s
EXTRACT-INTERVAL = (?i).+INTERVAL\:\s{0,}(?P<INTERVAL>\d+)\s
EXTRACT-SNAPSHOTS = (?i).+SNAPSHOTS\:\s{0,}(?P<SNAPSHOTS>\d+)\s
EXTRACT-logical_cpus = (?i).+logical_cpus\:\s{0,}(?P<logical_cpus>\d+)\s
EXTRACT-virtual_cpus = (?i).+virtual_cpus\:\s{0,}(?P<virtual_cpus>\d+)\s
EXTRACT-Nmon_ID = (?i).+NMON\sID\:\s{0,}(?P<Nmon_ID>[a-zA-Z0-9\-\:\,\_\.]+)\s
EXTRACT-serial = (?i).+SerialNumber:\s{0,}(?P<serial>[a-zA-Z0-9\-\_]+)\s

# Generate the frameID mapping
LOOKUP-nmon_frameID_mapping = nmon_frameID_mapping serialnum AS serial, host as host OUTPUTNEW frameID AS frameID, host_description as host_description

####################
# nmon config stanza
####################

[nmon_config]

# serial is extracted from the header added by the parsers.
# Notes: for AIX and PowerLinux, the serial number is as well available within the raw data
# unless it has been asked to do so, the parsers use the original serial for the header definition
EXTRACT-serial = CONFIG,[^,]*,[^,]*,(?P<serial>[a-zA-Z0-9\-\_]+)

# Perform basic extractions
# Full extractions can be performed by calling associated macros, or using the data model
EXTRACT-AAA_OS = (?i),OS,(?P<AAA_OS>[^,]+)\s{0,}
EXTRACT-AIX_LEVEL = AAA,AIX,(?P<AIX_LEVEL>[a-zA-Z0-9\-\_\.]+)\s
EXTRACT-Linux_LEVEL = AAA,OS,Linux,(?P<Linux_LEVEL>[^,]+),
EXTRACT-Solaris_LEVEL = AAA,OS,Solaris,(?P<Solaris_LEVEL>[^,]+),
EVAL-OStype = case(AAA_OS == "Linux", "Linux", AAA_OS == "Solaris", "Solaris", isnotnull(AIX_LEVEL), "AIX", isnull(AAA_OS), "Unknown")

# When applicable, be CIM compliant

FIELDALIAS-dest = host as dest
EXTRACT-AAA_serial = AAA,SerialNumber,(?<AAA_serial>\w*)
EVAL-serial = if(isnotnull(AAA_serial), AAA_serial, host)
EVAL-hypervisor_id = if(isnotnull(AAA_serial), AAA_serial, hostname)
EVAL-hypervisor = if(isnotnull(AAA_serial), AAA_serial, hostname)

# family
EXTRACT-AIX_processor_type = BBB\w*,[^,]+,lsconf,\"Processor\sType:\s(?<AIX_processor_type>\w*)\"
EXTRACT-Linux_processor_type = AAA,OS,Linux,[^,]+,[^,]+,(?<Linux_processor_type>\w*)
EXTRACT-Solaris_processor_type = BBB.+,[0-9].+psrinfo\s\-pv,\"\s+(?P<Solaris_processor_type>[\w\-\_]*)\s*\(.+\"
EVAL-family = case(isnotnull(Linux_LEVEL), Linux_processor_type, isnotnull(Solaris_LEVEL), Solaris_processor_type, isnotnull(AIX_LEVEL), AIX_processor_type)

# Vendor / Product
EXTRACT-Linux_release_distribution = BBB\w*,[^,]*,.+etc+.release,\"(?!LSB_VERSION|DISTRIB|NAME|ID|VERSION)(?P<Linux_release_distribution>[^,]+)\"
EXTRACT-Linux_lsb_distribution = BBB\w*,[^,]*,lsb_release,\"Description:\s*(?<Linux_lsb_distribution>[^,]*)\"
EVAL-Linux_distribution = if(isnotnull(Linux_lsb_distribution), Linux_lsb_distribution, Linux_release_distribution)
EXTRACT-Solaris_version = BBB\w*,[^,]*,.+etc+.release,\"\s+(?P<Solaris_version>Oracle\s*Solaris\s[^\"]*)\"
EVAL-AIX_version = case(isnotnull(AIX_LEVEL), "IBM AIX" + " " + AIX_LEVEL)

EVAL-vendor_product = case(isnotnull(Linux_LEVEL), (if(isnotnull(Linux_lsb_distribution), Linux_lsb_distribution, Linux_release_distribution)), isnotnull(Solaris_LEVEL), Solaris_version, isnotnull(AIX_LEVEL), "IBM AIX " + 'AIX_LEVEL' )
EVAL-version = case(isnotnull(Linux_LEVEL), Linux_LEVEL, isnotnull(Solaris_LEVEL), Solaris_LEVEL, isnotnull(AIX_LEVEL), AIX_LEVEL )
EVAL-os = case(isnotnull(Linux_LEVEL), (if(isnotnull(Linux_lsb_distribution), Linux_lsb_distribution, Linux_release_distribution)), isnotnull(Solaris_LEVEL), Solaris_version, isnotnull(AIX_LEVEL), "IBM AIX " + 'AIX_LEVEL' )

# CPU
EXTRACT-AIX_virtualcpus = BBB\w*,[^,]+,lparstat\s-i,\"Online\sVirtual\sCPUs\s*:\s*(?<AIX_virtualcpus>\d*)\"
EXTRACT-cpu_cores_position1 = AAA,cpus,(?P<cpu_cores_position1>\d+)
EXTRACT-cpu_cores_position2 = AAA,cpus,\d+,(?P<cpu_cores_position2>\d+)
EVAL-cpu_cores_combo = AIX_virtualcpus . " / " . cpu_cores_position2

EVAL-cpu_cores = if(isnotnull(AIX_virtualcpus), AIX_virtualcpus, cpu_cores_position1)
EVAL-cpu_count = case(isnotnull(AIX_LEVEL), AIX_virtualcpus, isnotnull(Solaris_LEVEL), cpu_cores_position1, isnotnull(Linux_LEVEL), cpu_cores_position1)

EXTRACT-Solaris_processor_clockspeed = BBB.+,[0-9].+psrinfo\s\-pv,.+clock\s(?P<Solaris_processor_clockspeed>[\d\.]*)\s*\w*\)\"
EXTRACT-Linux_processor_clockspeed_1 = AAA,[^,]+,MHz,(?P<Linux_processor_clockspeed_1>[\d\.]*)
EXTRACT-Linux_processor_clockspeed_2 = BBB\w*,[^,]+,/proc/cpuinfo,\"cpu\s*MHz\s*:\s*(?<Linux_processor_clockspeed_2>[\d\.]*)\"
EXTRACT-Linux_processor_clockspeed_3 = BBB\w*,[^,]+,/proc/cpuinfo,\"clock\s{0,}:\s{0,}(?<Linux_processor_clockspeed_3>[\d|\.]*)\s{0,}MHz\"
EXTRACT-AIX_processor_clockspeed = BBBP,[^,]+,lsconf,"Processor\sClock\sSpeed:\s*(?<AIX_processor_clockspeed>[\d\.]*)\sMHz
EVAL-cpu_mhz = case(isnotnull(Linux_LEVEL), case(isnotnull(Linux_processor_clockspeed_1), Linux_processor_clockspeed_1, isnotnull(Linux_processor_clockspeed_2), Linux_processor_clockspeed_2, isnotnull(Linux_processor_clockspeed_3), Linux_processor_clockspeed_3), isnotnull(Solaris_LEVEL), Solaris_processor_clockspeed, isnotnull(AIX_LEVEL), AIX_processor_clockspeed)

EXTRACT-Linux_realmemory_KB = BBB\w*,[^,]*,/proc/meminfo,\"MemTotal:\s*(?<Linux_realmemory_KB>\d*)\s*kB\"
EXTRACT-Solaris_realmemory_KB = BBB\w*,[^,]*,prtdiag,\"Memory\s*size:\s*(?<Solaris_realmemory_MB>\d*)\sMegabytes\"
EXTRACT-AIX_realmemory_online_MB = BBB\w*,[^,]*,online\s*Memory,(?<AIX_realmemory_online_MB>\d*)
EXTRACT-AIX_realmemory_lsconf_MB = BBB\w*,[^,]*,lsconf,\"Memory\s*Size:\s*(?<AIX_realmemory_lsconf_MB>\d*)\s*MB\"
EVAL-mem = case(isnotnull(Linux_LEVEL), round((Linux_realmemory_KB/1024),0), isnotnull(Solaris_LEVEL), Solaris_realmemory_MB, isnotnull(AIX_realmemory_online_MB), if(isnotnull(AIX_realmemory_online_MB), AIX_realmemory_online_MB, AIX_realmemory_lsconf_MB) )

# Not in CIM
EXTRACT-Linux_swapmemory_KB = BBB\w*,[^,]*,/proc/meminfo,\"SwapTotal:\s*(?<Linux_swapmemory_KB>\d*)\s*kB\"

# Processor extraction
EXTRACT-AIX_processor = BBB\w*,[0-9]*,lsconf,\"Processor\s*Type:\s*(?P<AIX_processor>[^\"]*)\"
EXTRACT-Linux_arch = BBB\w*,[0-9]*,lscpu,\"(Architecture)\s*:\s+(?P<Linux_arch>[^\"]*)\"
EXTRACT-Linux_processor_lscpu = BBB\w*,[0-9]*,lscpu,\"(Model\sname|Model|Vendor\sID)\s*:\s*(?P<Linux_processor_lscpu>[^\"]*)\"
EXTRACT-Linux_processor_cpuinfo = BBB\w*,[0-9].*cpuinfo,\"(model\sname)\s*:\s*(?P<Linux_processor_cpuinfo>[^\"]*)\"
EXTRACT-Solaris_processor_primary = BBB.+,[0-9].+psrinfo\s\-pv,\"\s+(?P<Solaris_processor_primary>.+)\s*\(.+clock.+\"
EXTRACT-Solaris_processor_alt = BBB.+,[0-9].+prtdiag,\"0\s*(?<Solaris_processor_clock_alt>[\d|\.]*\s*\w*)\s*(?P<Solaris_processor_alt>[\w|-]*)\s*
EVAL-processor = case(isnotnull(AIX_LEVEL), AIX_processor, AAA_OS == "Solaris", case(isnotnull(Solaris_processor_primary), Solaris_processor_primary, isnotnull(Solaris_processor_alt), Solaris_processor_alt), AAA_OS == "Linux", case(isnotnull(Linux_processor_lscpu), Linux_processor_lscpu + " (arch: " + Linux_arch + ")" , isnull(Linux_processor_lscpu), Linux_processor_cpuinfo) )

# uptime
# Sic !!!
EXTRACT-uptime_days = \s{0,}[\d|\w|:]*[\s|_]*up[\s|_]*(?<uptime_days>\d*)[\s|_]*day[s|,|;]*[\s|_]*(?<uptime_dayshour>\d*):(?<uptime_daystime>\d*)
EXTRACT-uptime_days_only_alt = \s*\d\d:\d\d(?:s|AM|PM)\s*up\s*(?<uptime_days>\d*)\s*day
EXTRACT-uptime_days_with_min = \s{0,}[\d|\w|:]*[\s|_]*up[\s|_]*(?<uptime_days>\d*)[\s|_]*day[s|,|;]*[\s|_]*(?<uptime_daysmin>\d*)[\s|_]min
EXTRACT-uptime_days_with_min_alt = \s{0,}\d\d:\d\d(?:s|AM|PM)\s{0,}up\s{0,}(?<uptime_days>\d*)\s*(?:s|day)(?:s|days);\s{0,}(?<uptime_daysmin>\d*)\s{0,}(?:s|hr)(?:s|hrs)
EXTRACT-uptime_minutes = \s{0,}[\d|\w|:]*[\s|_]*up[\s|_]*(?<uptime_minutes>\d*)[\s|_]*min[\s|_]*[,|;]
EXTRACT-uptime_minutes_alt = \s*\d\d:\d\d(?:s|AM|PM)\s*up\s*(?<uptime_minutes>\d*)\s*(?:s|min)(?:s|mins)
EXTRACT-uptime_hours = \s{0,}[\d|\w|:]*[\s|_]*up[\s|_]*(?<uptime_hours>\d*:\d*)[\s|_]*[,|;]
EXTRACT-uptime_hours_alt = \s*\d\d:\d\d(?:s|AM|PM)\s*up\s*(?<uptime_hours>\d*)\s*hr
EXTRACT-uptime_hours_hours = \s{0,}[\d|\w|:]*[\s|_]*up[\s|_]*(?<uptime_hours_hours>\d*):\d*[\s|_]*[,|;]
EXTRACT-uptime_hours_minutes = \s{0,}[\d|\w|:]*[\s|_]*up[\s|_]*\d*:(?<uptime_hours_minutes>\d*)[\s|_]*[,|;]
EVAL-uptime_fromdays = (uptime_days*86400)
EVAL-uptime_fromminutes = (uptime_minutes*60)
EVAL-uptime_fromhours = (uptime_hours_hours*60*60) + (uptime_hours_minutes*60)
EVAL-uptime = case(isnotnull(uptime_daysmin) AND isnotnull(uptime_daysmin), ((uptime_days*86400)+(uptime_daysmin*60)),\
isnotnull(uptime_days) AND isnotnull(uptime_dayshour) AND isnotnull(uptime_daystime), ((uptime_days*86400)+(uptime_dayshour*3600)+(uptime_daystime*60)),\
isnotnull(uptime_minutes), (uptime_minutes*60),\
isnotnull(uptime_hours) AND isnotnull(uptime_hours_hours), (uptime_hours_hours*60*60) + (uptime_hours_minutes*60),\
isnotnull(uptime_hours), (uptime_hours*60*60),\
isnotnull(uptime_days), (uptime_days*86400) )

# CIM Network
REPORT-inventory_network_extractions = inventory_network_interface, inventory_network_ip, inventory_network_mac

# Generate the frameID mapping
LOOKUP-nmon_frameID_mapping = nmon_frameID_mapping serialnum AS serial, host as host OUTPUTNEW frameID AS frameID, host_description as host_description

# For nmon_config sent over http
[source::nmon_config:http]
KV_MODE=none